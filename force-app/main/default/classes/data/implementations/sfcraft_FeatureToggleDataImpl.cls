public inherited sharing class sfcraft_FeatureToggleDataImpl implements sfcraft_FeatureToggleData {
    public sfcraft_FeatureToggleDTO getCurrentUserSettingFor(
        String featureName
    ) {
        sfcraft_FeatureToggleSettings__c setting = sfcraft_FeatureToggleSettings__c.getInstance();
        Boolean value = (Boolean) setting.get(featureName + '__c');
        sfcraft_FeatureToggleDTO result = new sfcraft_FeatureToggleDTO(
            featureName,
            value
        );
        return result;
    }
}
