public interface sfcraft_FeatureToggleData {
    sfcraft_FeatureToggleDTO getCurrentUserSettingFor(String featureName);
}
