public inherited sharing class sfcraft_FeatureToggleDTO {
    public String featureName { get; private set; }
    public Id scope { get; private set; }
    public Boolean isEnabled { get; private set; }

    public sfcraft_FeatureToggleDTO(String featureName, Boolean isEnabled) {
        this.featureName = featureName;
        this.isEnabled = isEnabled;
    }

    public sfcraft_FeatureToggleDTO(
        String featureName,
        Boolean isEnabled,
        Id scopeRecordId
    ) {
        this.featureName = featureName;
        this.isEnabled = isEnabled;
        this.scope = scopeRecordId;
    }
}
