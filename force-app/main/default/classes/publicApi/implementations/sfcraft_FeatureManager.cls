public without sharing class sfcraft_FeatureManager implements sfcraft_FeatureToggle.Manager {
    private static Map<SObjectType, sfcraft_FeatureToggle.ScopeType> scopeTypeMapping = new Map<SObjectType, sfcraft_FeatureToggle.ScopeType>{
        Organization.getSObjectType() => sfcraft_FeatureToggle.ScopeType.ORG,
        Profile.getSObjectType() => sfcraft_FeatureToggle.ScopeType.PROFILE,
        User.getSObjectType() => sfcraft_FeatureToggle.ScopeType.USER
    };

    private static Map<String, sfcraft_FeatureToggle.Feature> overrides = new Map<String, sfcraft_FeatureToggle.Feature>();

    private sfcraft_FeatureToggleData featureData;

    public sfcraft_FeatureManager(sfcraft_FeatureToggleData featureData) {
        this.featureData = featureData;
    }

    public void overrideFeatureToggle(String name, Boolean value) {
        Feature toggleOverride = new Feature(name, value);
        toggleOverride.setIsOverride(true);
        overrides.put(name, toggleOverride);
    }

    public sfcraft_FeatureToggle.Feature getFeatureSetting(String name) {
        if (overrides.containsKey(name)) {
            return overrides.get(name);
        }

        try {
            sfcraft_FeatureToggleDTO featureToggleDTO = featureData.getCurrentUserSettingFor(
                name
            );
            return new Feature(featureToggleDTO);
        } catch (SObjectException ex) {
            return new Feature(name);
        }
    }

    private class Feature implements sfcraft_FeatureToggle.Feature {
        private String featureName;
        private sfcraft_FeatureToggleDTO toggle;
        private Boolean isOverride;

        private Feature() {
            this.isOverride = false;
        }

        public Feature(String featureName, Boolean overridenValue) {
            this();
            this.toggle = new sfcraft_FeatureToggleDTO(
                featureName,
                overridenValue
            );
        }

        public Feature(String featureName) {
            this();
            this.toggle = new sfcraft_FeatureToggleDTO(featureName, false);
        }

        public Feature(sfcraft_FeatureToggleDTO toggle) {
            this();
            this.toggle = toggle;
        }

        public String name() {
            return this.toggle.featureName;
        }

        public sfcraft_FeatureToggle.ScopeType scope() {
            if (this.isOverride) {
                return sfcraft_FeatureToggle.ScopeType.OVERRIDEN;
            }
            return scopeTypeMapping.get(this.scopeRecordId().getSObjectType());
        }

        public Id scopeRecordId() {
            Id scopeId = this.toggle.scope;
            if (null == scopeId) {
                scopeId = UserInfo.getOrganizationId();
            }
            return scopeId;
        }

        public Boolean isOn() {
            return this.toggle.isEnabled;
        }

        private void setIsOverride(Boolean isOverride) {
            this.isOverride = isOverride;
        }
    }
}
