@isTest
private class sfcraft_FeatureManagerTest {
    private static FakeData featureData;
    private static sfcraft_FeatureToggle.Manager manager;

    private static sfcraft_FeatureToggleDTO orgWideToggle = new sfcraft_FeatureToggleDTO(
        'super feature',
        true
    );

    private static sfcraft_FeatureToggleDTO profileToggle = new sfcraft_FeatureToggleDTO(
        'super feature',
        false,
        UserInfo.getProfileId()
    );

    private static sfcraft_FeatureToggleDTO userToggle = new sfcraft_FeatureToggleDTO(
        'super feature',
        true,
        UserInfo.getUserId()
    );

    static {
        featureData = new FakeData();
        manager = new sfcraft_FeatureManager(featureData);
    }

    @isTest
    static void featuresAreDisabledByDeafult() {
        String featureName = 'feature name';

        manager = new sfcraft_FeatureManager(
            new sfcraft_FeatureToggleDataImpl()
        );
        sfcraft_FeatureToggle.Feature featureSetting = manager.getFeatureSetting(
            featureName
        );

        System.assertEquals(
            featureName,
            featureSetting.name(),
            'Feature name is different from expected'
        );
        System.assertEquals(
            false,
            featureSetting.isOn(),
            'Feature should be disabled by default'
        );
    }

    @isTest
    static void ifScopeIsNotSpecifiedOntheRecord_ConsiderItOrgWide() {
        featureData.setFakeRecord(orgWideToggle);

        sfcraft_FeatureToggle.Feature featureSetting = manager.getFeatureSetting(
            orgWideToggle.featureName
        );
        System.assertEquals(orgWideToggle.featureName, featureSetting.name());
        System.assertEquals(orgWideToggle.isEnabled, featureSetting.isOn());
        System.assertEquals(
            sfcraft_FeatureToggle.ScopeType.ORG,
            featureSetting.scope()
        );
        System.assertEquals(
            UserInfo.getOrganizationId(),
            featureSetting.scopeRecordId()
        );
    }

    @isTest
    static void profileSettingOverridesOrgSetting() {
        featureData.setFakeRecord(profileToggle);

        sfcraft_FeatureToggle.Feature featureSetting = manager.getFeatureSetting(
            orgWideToggle.featureName
        );
        System.assertEquals(profileToggle.featureName, featureSetting.name());
        System.assertEquals(profileToggle.isEnabled, featureSetting.isOn());
        System.assertEquals(
            sfcraft_FeatureToggle.ScopeType.PROFILE,
            featureSetting.scope()
        );
        System.assertEquals(
            UserInfo.getProfileId(),
            featureSetting.scopeRecordId()
        );
    }

    @isTest
    static void userSettingOverridesOrgSetting() {
        featureData.setFakeRecord(userToggle);

        sfcraft_FeatureToggle.Feature featureSetting = manager.getFeatureSetting(
            orgWideToggle.featureName
        );
        System.assertEquals(userToggle.featureName, featureSetting.name());
        System.assertEquals(userToggle.isEnabled, featureSetting.isOn());
        System.assertEquals(
            sfcraft_FeatureToggle.ScopeType.USER,
            featureSetting.scope()
        );
        System.assertEquals(
            UserInfo.getUserId(),
            featureSetting.scopeRecordId()
        );
    }

    @isTest
    static void canOverrideAnyToggleForTransaction() {
        featureData.setFakeRecord(profileToggle);

        manager.overrideFeatureToggle(orgWideToggle.featureName, true);

        sfcraft_FeatureToggle.Feature featureSetting = manager.getFeatureSetting(
            orgWideToggle.featureName
        );
        System.assertEquals(userToggle.featureName, featureSetting.name());
        System.assertEquals(userToggle.isEnabled, featureSetting.isOn());
        System.assertEquals(
            sfcraft_FeatureToggle.ScopeType.OVERRIDEN,
            featureSetting.scope()
        );
        System.assertEquals(
            UserInfo.getOrganizationId(),
            featureSetting.scopeRecordId()
        );
    }

    private class FakeData implements sfcraft_FeatureToggleData {
        sfcraft_FeatureToggleDTO record;

        public void setFakeRecord(sfcraft_FeatureToggleDTO record) {
            this.record = record;
        }

        public sfcraft_FeatureToggleDTO getCurrentUserSettingFor(
            String featureName
        ) {
            return this.record;
        }
    }
}
