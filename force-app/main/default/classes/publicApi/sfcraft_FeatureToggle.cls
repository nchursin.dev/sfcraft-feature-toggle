public without sharing class sfcraft_FeatureToggle {
    public enum ScopeType {
        USER,
        PROFILE,
        ORG,
        OVERRIDEN
    }

    public interface Manager {
        Feature getFeatureSetting(String name);
        void overrideFeatureToggle(String name, Boolean value);
    }

    public interface Feature {
        String name();
        ScopeType scope();
        Id scopeRecordId();
        Boolean isOn();
    }

    public static Manager getFeatureManagerInstance() {
        return new sfcraft_FeatureManager(new sfcraft_FeatureToggleDataImpl());
    }
}
